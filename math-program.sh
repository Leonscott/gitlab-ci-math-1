#  Author FISO
#accepts 3 numbers as command line arguments (NUM1, NUM2, NUM3)
#Calculates a result that is equal to (NUM1 ^ NUM2) + NUM3 x NUM2
#The program should print the calculation it is about to perform in the format (NUM1 ^ NUM2) + NUM3 x NUM2


VAR1=$3
VAR2=$2
VAR3=$4

if [ -z ${OUTPUT_FILE_NAME} ] 
then
    
    OUTPUT_FILE_NAME="output.txt"
    echo "Using default output file name of '$OUTPUT_FILE_NAME'."
else
    echo "Using given file name of '$OUTPUT_FILE_NAME'"
fi

echo "We will attempt to calculate (($VAR1 ^ $VAR2) + $VAR3 * $VAR2)"


echo "We have done a lot of work" > $OUTPUT_FILE_NAME


RESULT=$(((VAR1**VAR2) + VAR3 * VAR2))


RESULT_OUTPUT="Calculated result : $VAR1 ^ $VAR2 + $VAR3 * $VAR2 = $RESULT"

echo $RESULT_OUTPUT

echo $RESULT_OUTPUT  >> $OUTPUT_FILE_NAME

